package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.User;

/**
 * ユーザテーブル用のDao
 *
 * @author takano
 */
public class UserDao {

  /**
   * ログインIDとパスワードに紐づくユーザ情報を返す S
   * 
   * @param loginId
   * @param password
   * @return
   */
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String _loginId = rs.getString("login_id"); // 変数名がメソッドの引数と同じになってしまうため、別の名前(_loginId)を設定する
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String _password = rs.getString("password"); // 変数名がメソッドの引数と同じになってしまうため、別の名前を設定する
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");
      return new User(id, _loginId, name, birthDate, _password, isAdmin, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  /**
   * 全てのユーザ情報を取得する
   *
   * @return
   */
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      // TODO: 未実装：管理者以外を取得するようSQLを変更する
      String sql = "SELECT * FROM user WHERE is_admin = false";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  public void insert(String loginId, String name, String birth, String password) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();
      String sql =
          "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date)VALUES(?,?,?,?,now(),now())";
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, name);
      pStmt.setString(3, birth);
      pStmt.setString(4, password);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
  }

  public User findById(int idint) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      // SQLでWhere してidと一致したデータＳＥＬＥＣＴしてdetailに代入
      conn = DBManager.getConnection();
      String sql = "SELECT * FROM user WHERE id=?";

      pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, idint);
      ResultSet rs = pStmt.executeQuery();
      
      if (!rs.next()) {
        return null;
      }

      int id = rs.getInt("id");
      String loginId = rs.getString("login_id");
      String name = rs.getString("name");
      Date birthDate = rs.getDate("birth_date");
      String password = rs.getString("password");
      boolean isAdmin = rs.getBoolean("is_admin");
      Timestamp createDate = rs.getTimestamp("create_date");
      Timestamp updateDate = rs.getTimestamp("update_date");

      User User = new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);
      return User;

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  public void update(int id, String pass, String name, String birth) {
    // TODO 自動生成されたメソッド・スタブ
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      conn = DBManager.getConnection();
      String sql =
          "UPDATE user SET password = ?,name = ? ,birth_date = ?,update_date = now() WHERE id = ?";

      pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, pass);
      pStmt.setString(2, name);
      pStmt.setString(3, birth);
      pStmt.setInt(4, id);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public void delete(int id) {
    Connection conn = null;
    PreparedStatement pStmt = null;
    try {
      conn = DBManager.getConnection();
      String sql = "DELETE FROM user WHERE id = ?";

      pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, id);
      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

  public List<User> search(String loginid, String uname, String sdate, String edate) {
    List<User> userList = new ArrayList<User>();
    Connection conn = null;
    try {
      conn = DBManager.getConnection();
      StringBuilder sql = new StringBuilder("SELECT * FROM user WHERE is_admin = false");

      if (!loginid.equals("")) { // ログインIDの検索が入力されていたら
        sql.append(" AND ");
        sql.append("login_id = " + "\"" + loginid + "\"");
      }
      
      if (!uname.equals("")) { // nameの検索が入力されていたら
          sql.append(" AND ");
        sql.append("name LIKE" + "'%" + uname + "%'");
      }

      if (!sdate.equals("") && !edate.equals("")) {// 生年月日の開始と終了が入力されていたら
          sql.append(" AND ");
          sql.append("birth_date BETWEEN " + "\'" + sdate + "\'" + " AND " + "\'" + edate + "\'");
      } else if (!sdate.equals("")) {
          sql.append(" AND ");
          sql.append("birth_date >=" + "\'" + sdate + "\'");
      } else if (!edate.equals("")) {
          sql.append(" AND ");
          sql.append("birth_date <=" + "\'" + edate + "\'");
      }
      

      // 型変換
      String sqlex = sql.toString();
      // 実行
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sqlex);


      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }

      return userList;

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
