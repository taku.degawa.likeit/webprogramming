package controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import util.PasswordEncoder;

@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
  public UserAddServlet() {
    super();
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");
    HttpSession session = request.getSession();
    if ((session.getAttribute("userInfo")) == null) {
      response.sendRedirect("LoginServlet");
    } else {
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
    }
  }


  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    request.setCharacterEncoding("UTF-8");
    String loginId = request.getParameter("user-loginid");
    String password = request.getParameter("password");
    String checkPass = request.getParameter("password-confirm");
    String name = request.getParameter("user-name");
    String birth = request.getParameter("birth-date");
    int err = 0;
    
    if (loginId.equals("")) {
      err = 1;
    }
    if (!password.equals(checkPass)) {
      err = 1;
    } 
    if (name.equals("")) {
      err = 1;
    } 
    if (password.equals("") || checkPass.equals("")) {
      err = 1;
    } 
    if (birth.equals("")) {
      err = 1;
    }

    if (err == 1) {
      request.setAttribute("loginid", loginId);
      request.setAttribute("name", name);
      request.setAttribute("birth", birth);
      request.setAttribute("userAddErrMsg", "入力された内容は正しくありません");

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
      dispatcher.forward(request, response);
    }
    
    
      PasswordEncoder pe = new PasswordEncoder();
      String passen = pe.encordPassword(password);
      UserDao userDao = new UserDao();
      userDao.insert(loginId, name, birth, passen);
      response.sendRedirect("UserListServlet");
    }

  }


