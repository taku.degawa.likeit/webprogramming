package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;
import util.PasswordEncoder;

/**
 * Servlet implementation class UserUpdate
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        HttpSession session = request.getSession();
        if ((session.getAttribute("userInfo")) == null) {
          response.sendRedirect("LoginServlet");
        } else {
          String id = request.getParameter("id");
        // 確認用：idをコンソールに出力
          System.out.println(id);
          int idint = Integer.valueOf(id).intValue();

          UserDao userDao = new UserDao();
          User user = userDao.findById(idint);
        // TODO 未実装：ユーザ情報をリクエストスコープにセットしてjspにフォワード
          request.setAttribute("user", user);
          RequestDispatcher dispatcher =
              request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
          dispatcher.forward(request, response);
        }

      }

      /**
       * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
       */
      protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String id = request.getParameter("user-id");
        // 確認用：idをコンソールに出力
        System.out.println(id);
        int idint = Integer.valueOf(id).intValue();

        UserDao userDao = new UserDao();
        User user = userDao.findById(idint);
        request.setAttribute("user", user);

        String loginId = request.getParameter("login-id");
        String password = request.getParameter("password");
        String checkPass = request.getParameter("password-confirm");
        String name = request.getParameter("user-name");
        String birth = request.getParameter("birth-date");
        int err = 0;

        if (!password.equals(checkPass)) {
          err = 1;
        }
        if (name.equals("")) {
          err = 1;
        }
        if (birth.equals("")) {
          err = 1;
        }
        if (err == 1) {
          request.setAttribute("loginid", loginId);
          request.setAttribute("id", id);
          request.setAttribute("name", name);
          request.setAttribute("birth", birth);

          request.setAttribute("upErrMsg", "入力された内容は正しくありません");

          RequestDispatcher dispatcher =
              request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
          dispatcher.forward(request, response);
        } else {
          PasswordEncoder pe = new PasswordEncoder();
          String passen = pe.encordPassword(password);
          userDao.update(idint, passen, name, birth);
          userDao = new UserDao();
          List<User> userList = userDao.findAll();
        // リクエストスコープにユーザ一覧情報をセット
          request.setAttribute("userList", userList);
        // ユーザ一覧のjspにフォワード
          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
          dispatcher.forward(request, response);
      }
	}

}
