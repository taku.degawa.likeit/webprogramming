package controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.UserDao;
import model.User;

/** Servlet implementation class UserListServlet */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;
  final private static String URL = "jdbc:mysql://localhost/";
  final private static String DB_NAME = "usermanagement";
  final private static String USER = "root";
  final private static String PASS = "password";

  /** @see HttpServlet#HttpServlet() */
  public UserListServlet() {
    super();
  }

  /** @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response) */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    // TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
    HttpSession session = request.getSession();
    if ((session.getAttribute("userInfo")) == null) {
      response.sendRedirect("LoginServlet");
    } else {
      // ユーザ一覧情報を取得
      UserDao userDao = new UserDao();
      List<User> userList = userDao.findAll();

    // リクエストスコープにユーザ一覧情報をセット
      request.setAttribute("userList", userList);

    // ユーザ一覧のjspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);
    }
  }

  /** @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response) */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    Connection con = null;
    Statement stmt = null;
    
    try {
      Class.forName("com.mysql.jdbc.Driver");
      con = DriverManager.getConnection(URL + DB_NAME, USER, PASS);
    
    // TODO 未実装：検索処理全般
    request.setCharacterEncoding("UTF-8");
    String loginId = request.getParameter("user-loginid");
    String name = request.getParameter("user-name");
    String sdate = request.getParameter("date-start");
    String edate = request.getParameter("date-end");
    
    UserDao userDao = new UserDao();
    List<User> userList = userDao.search(loginId, name, sdate, edate);

    request.setAttribute("loginId", loginId);
    request.setAttribute("name", name);
    request.setAttribute("sdate", sdate);
    request.setAttribute("edate", edate);

      request.setAttribute("userList", userList);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
      dispatcher.forward(request, response);
    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    } finally {
      try {
        if (stmt != null) {
          stmt.close();
        }
        if (con != null) {
          con.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
}
