﻿create database usermanagement character set utf8;
use usermanagement;
create table user
                (id SERIAL  PRIMARY KEY UNIQUE NOT NULL,
                login_id varchar(255)UNIQUE NOT NULL,
                name varchar(255) NOT NULL,
                birth_date DATE NOT NULL,
                password varchar(255) NOT NULL,
                is_admin boolean default 0 NOT NULL,
                create_date DATETIME NOT NULL,
                update_date DATETIME NOT NULL);
                
use usermanagement;                
insert into user(login_id,name,birth_date,password,is_admin,create_date,update_date)
                 VALUES('admin','管理者','20010316','password',true,now(),now());
                 
INSERT INTO user(
    login_id,
    name,
    birth_date,
    password,
    create_date,
    update_date
)
VALUES(
    'user01',
    '一般1',
    '2001-12-31',
    'password',
    now(),
    now()
);

INSERT INTO user(
    login_id,
    name,
    birth_date,
    password,
    create_date,
    update_date
)
VALUES(
    'user02',
    '一般2',
    '2001-12-31',
    'password',
    now(),
    now()
);
INSERT INTO user(
    login_id,
    name,
    birth_date,
    password,
    create_date,
    update_date
)
VALUES(
    'user03',
    '一般3',
    '2001-12-31',
    'password',
    now(),
    now()
);

